# AVR

Programmation d'AVR du genre ATmega16 et ATmega328p. AVR veut dire «Alf and Vegard's RISC processor», soit le processeur RISC d'Alf et de Vegard qui sont les inventeurs de l'architecture.

RISC veut dire «Reduced instruction set computer», soit processeur à jeu d'instructions réduit. [Wikipedia au sujet de RISC](https://fr.wikipedia.org/wiki/Processeur_%C3%A0_jeu_d%27instructions_r%C3%A9duit)

Ce sont des processeurs 8 bits. Ils fonctionnent le plus efficacement avec des données d'une longueur de 8 bits. Avec 8 bits, on peut des valeurs de 0 à 255.

## Installation

### Environnement de développement

Il y a des scripts d'installation pour configurer un environnement de développement à partir de presque zéro dans [le dossier installation](installation). Il faut au moins un ordinateur avec Debian, préférablement une version récente comme 11.5. D'autres distributions de Linux fonctionneront aussi.

Il y a des manières de programmer sous Windows, mais je ne les couvre pas.

### Écriture du programme sur l'AVR

Il faut aller dans [le dossier source](source) et ensuite ouvrir le dossier qui a le même nom que le modèle d'AVR vers lequel on veut envoyer le programme.

