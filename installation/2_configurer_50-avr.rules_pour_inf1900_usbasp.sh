#!/bin/bash

#############################################################################################################
# Le script suivant va créer une règle et l'écrire dans le fichier 50-avr.rules.
# Ceci permettra à l'utilisateur courant de pouvoir programmer l'AVR avec avrdude.
#
# Copyright (C) 2022 Alpaca Balena.
#
# Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
# Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
# licence, soit (à vous de voir...) toute autre version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
# implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
# connaissance de la Licence Publique Générale GNU pour plus de détails.
#
# Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
# cas, voir <https://www.gnu.org/licenses/>.
#############################################################################################################
# Cette règle est pour la carte de programmation de type usbasp créée pour le cours INF1990 de l'école
# Polytechnique de Montréal.
# Plus de détails sur le cours d'INF1900 ici https://cours.polymtl.ca/inf1900/
# Plus de détails sur le code du cours INF1900 ici https://github.com/wtrep/INF1900
# Plus de détails sur usbasp ici https://www.fischl.de/usbasp/
RULE='SUBSYSTEM=="usb", ATTR{idVendor}=="16c0", ATTR{idProduct}=="05dc", OWNER="'$USER'", MODE="0666", SYMLINK+="inf1900-board"'
echo $RULE > 50-avr.rules
sudo mv 50-avr.rules /etc/udev/rules.d/
sudo /etc/init.d/udev restart
sudo ls -la /dev/ | grep inf1900-board | wc -l
#sudo dmesg # Cette command produit trop de messages, mais elle peut être utile our déboguer.
#sudo lsusb # Celle-ci aussi.

