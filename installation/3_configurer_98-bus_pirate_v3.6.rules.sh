#!/bin/bash

#############################################################################################################
# Le script suivant va créer une règle et l'écrire dans le fichier 98-buspirate.rules.
# Ceci permettra à l'utilisateur courant de pouvoir utiliser le bus pirate avec screen sous linux et aussi
# comme programmeur d'AVR avec avrdude.
#
# Copyright (C) 2022 Alpaca Balena.
#
# Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
# Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
# licence, soit (à vous de voir...) toute autre version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
# implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
# connaissance de la Licence Publique Générale GNU pour plus de détails.
#
# Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
# cas, voir <https://www.gnu.org/licenses/>.
#############################################################################################################
# Plus de détails sur le buspirate ici http://dangerousprototypes.com/docs/Bus_Pirate
RULE='SUBSYSTEM=="usb", ATTR{idVendor}=="0403", ATTR{idProduct}=="6001", ATTR{serial}=="AJ02XHJD", OWNER="'$USER'", MODE="0666", SYMLINK+="bus_pirate_v3.6"'
echo $RULE > 98-bus_pirate_v3.6.rules
sudo mv 98-bus_pirate_v3.6.rules /etc/udev/rules.d/
sudo /etc/init.d/udev restart
sudo ls -la /dev/ | grep bus_pirate_v3.6 | wc -l
#sudo dmesg # Cette command produit trop de messages, mais elle peut être utile au débogage.
#sudo lsusb # Celle-ci aussi.

