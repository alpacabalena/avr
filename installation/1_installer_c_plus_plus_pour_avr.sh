#!/bin/bash

##############################################################################
# Ce script utilise le fichier pkglist.txt pour commander à linux d'installer
# tous les programmes nécessaires pour compiler des programmes en C++ et les
# envoyer à un AVR.
#
# Copyright (C) 2022 Alpaca Balena.
#
# Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon
# les termes de la Licence Publique Générale GNU telle que publiée par la Free
# Software Foundation ; soit la version 3 de cette licence, soit (à vous de
# voir...) toute autre version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE
# GARANTIE, ni explicite ni implicite, y compris les garanties de 
# commercialisation ou d'adaptation dans un but spécifique. Prenez connaissance
# de la Licence Publique Générale GNU pour plus de détails.i
#
# Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce
# programme ; si ce n'est pas le cas, voir <https://www.gnu.org/licenses/>.
##############################################################################

# N'attends pas de réponses de l'utilisateur.
export DEBIAN_FRONTEND=noninteractive && \

# Mets à jour les endroits d'où proviennent les programmes.
sudo apt-get update && \

# Mets à jour les programmes déjà installés.
sudo apt-get dist-upgrade -y && \

# Installe les programmes contenus dans le fichier pkglist.txt.
sudo apt-get install --yes $(cat pkglist.txt)
