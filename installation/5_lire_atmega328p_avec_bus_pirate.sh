#!/bin/bash

#############################################################################################################
# Ce script permet de lire les fusibles d'un AVR ATmega328p. Si ça fonctionne et que le microcontrôleur est
# reconnu par le bus pirate, on peut ensuite utiliser avrdude pour le programmer.
#
# Copyright (C) 2022 Alpaca Balena.
#
# Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
# Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
# licence, soit (à vous de voir...) toute autre version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
# implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
# connaissance de la Licence Publique Générale GNU pour plus de détails.
#
# Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
# cas, voir <https://www.gnu.org/licenses/>.
#############################################################################################################
# Voir dans le dossier "exemples" un résultat possible.
avrdude -c buspirate -P /dev/ttyUSB0 -p atmega328p -v
