#!/bin/bash

#############################################################################################################
# Ce script permet de trouver les détails du bus pirate
#
# Copyright (C) 2022 Alpaca Balena.
#
# Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
# Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
# licence, soit (à vous de voir...) toute autre version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
# implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
# connaissance de la Licence Publique Générale GNU pour plus de détails.
#
# Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
# cas, voir <https://www.gnu.org/licenses/>.
#############################################################################################################
# Décommenter les lignes suivantes pour voir les appareils USB qui sont connectés et déconnectés.
#echo "Écoute des périphériques USB... Brancher le bus pirate pour voir les détails."
#udevadm monitor --environment --udev

# Lire les dispositifs USB connectés.
lsusb

# Entrer le chemin vers le bus pirate trouvé en lisant ce que la commande lsusb aura retournée.
echo "Écrire le chemin vers le bus pirate. Le format est /dev/bus/usb/abc/def où abc est le Bus et def est le Device pour Future Technology Devices International, Ltd FT232 Serial (UART) IC.:  "
read chemin_usb
# Écrire les informations lue à partir de udevadm sur le bus pirate. Voir un exemple dans le dossiers exemples.
udevadm info -a -p $(udevadm info -q path -n /dev/bus/usb/$chemin_usb) > bus_pirate_v3.6_udevadm_info_script.txt
