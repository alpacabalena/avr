#!/bin/bash

#############################################################################################################
# Ce script permet de vérifier si le bus pirate est connecté.
#
# Copyright (C) 2022 Alpaca Balena.
#
# Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
# Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
# licence, soit (à vous de voir...) toute autre version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
# implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
# connaissance de la Licence Publique Générale GNU pour plus de détails.
#
# Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
# cas, voir <https://www.gnu.org/licenses/>.
#############################################################################################################
sudo ls -la /dev/ | grep bus_pirate_v3.6
# Décommenter pour plus d'information de débogage.
#sudo dmesg
#sudo lsusb
