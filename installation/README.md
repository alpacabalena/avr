# installation

## Étapes

Les scripts sont numérotés et ils contiennent chacun une description de ce qu'ils font au tout début. Il y a des commentaires sur la plupart des lignes de commande pour comprendre ce qu'elles font.

J'ai mis des exemples du résultat attendu pour certaines des commandes. Ça pourra être utile pour déboguer sur certains ordinateurs qui seraient configurés différement.

J'ai utilisé Debian 11.4 AMD64 avec ces scripts.
