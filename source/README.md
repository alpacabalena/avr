# Code Source
Le code qu'on peut compiler pour ensuite installer sur un microcontrôleur avec avrdude.
## Liste des programmes existants
### ATmega16
- allumer_del: pour allumer une ou plusieurs DELs en utilisant une minuterie.
- thermostat: pour mesurer un voltage qui varie en fonction de la température. Il y a un diagramme du circuit [ici dans le dossier Thermostat](https://1drv.ms/u/s!AkG0zvsHuKjnbRMYYgwZuAQ8aWo?e=n3actF). Un document .xcf pour gimp et un jpeg.
