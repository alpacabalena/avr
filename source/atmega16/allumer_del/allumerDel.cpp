/************************************************************************************************************
 * Utilisation du comparateur A de la minuterie 1 du ATmega16. Le programme peut être amélioré et simplifié.
 * Mon idée est d'avoir un mélange de valeurs binaires, hexadécimales et d'utiliser aussi le nom des pattes
 * lui-même pour montrer différentes manières possibles de faire des opérations binaires.
 * Il existe aussi un registre configurable dans l'ATmega16 pour dire d'inverser la valeur d'une patte OC1A
 * ce qui permettrait de retirer la routine d'interruption au complet.
 *
 * Copyright (C) 2022 Alpaca Balena.
 *
 * Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
 * Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
 * licence, soit (à vous de voir...) toute autre version ultérieure.
 *
 * Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
 * implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
 * connaissance de la Licence Publique Générale GNU pour plus de détails.
 *
 * Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
 * cas, voir <https://www.gnu.org/licenses/>.
************************************************************************************************************/
#include <avr/io.h> 
#include <avr/interrupt.h>

ISR(TIMER1_COMPA_vect)
{
    // ~(1 << PD7) est égal à 0b01111111. Il sert à remettre la patte PD7 à 0.
    // (1 << PD7) est égal à 0b10000000. Il sert à sélectionner seulement la valeur de PD7 quand on fait &.
    PORTD = (PORTD & 0b01111111) | (~(PORTD & 0b10000000) & 0b10000000);
    // Mêmes opérations pour PD6.
    PORTD = (PORTD & 0b10111111) | (~(PORTD & 0b01000000) & 0b01000000);
    PORTC = ~PORTC;
}

int main()
{
    // Désactive le JTAG pour pouvoir utiliser le PORTC. On doit écrire deux
    // fois en dedans de 4 cycle d'horloge.
    MCUCSR = (1 << JTD);
    MCUCSR = (1 << JTD);
    // Quand MCUCSR ne fonctionne pas, il faut changer les fusibles/fuses de l'ATmega. FAIRE ATTENTION DE NE
    // PAS SE TROMPER SINON, l'ATmega16 pourrait devenir une "brique" que seul un programmeur parallèle 12V
    // serait en mesure de ramener. Pour déprogrammer le JTAGEN et le désactiver, on écrit 1 dans JTAGEN:
    // avrdude -c usbasp -p atmega16 -P /dev/ttyS0 -U hfuse:w:0xD9:m
    // Il faut exécuter la commande lorsque le usbasp inf1990 est connecté à l'atmega16. FAITES TRÈS ATTENTION
    // DE DOUBLE-VÉRIFIER AVEC LE MANUEL DU ATMEGA16 ET REMPLACEZ LA VALEUR 0xD9 PAR CE QUI VOUS CONVIENT.
    // À VOS RISQUES ET PÉRILS!

	DDRD = 0xff;  //PORTD en sortie.
    // PD7 commence à 1. PD6 commence à 0. Ils clignoteront inversement. PD0 sera toujours allumé.
    PORTD = (1 << PD7) | (1 << PD0);
	DDRC = 0xff;
	PORTC = 0xff;

	sei();  // Activation des interruptions.
    TIMSK = (1 << OCIE1A);  // Activation de l'interruption du Timer1 sur comparaison.

    OCR1A = 0x1e88;  // 8MHz/1024 = 7812,5 Hz donc, 7812 ou 7813 pour une seconde, soit 0x1e84.
    TCCR1A = (1 << COM1A0);  // Activation de la comparaison.
    TCCR1B = (1 << WGM12) | (1 << CS12) | (1 << CS10); // Démarrer le Timer1 en divisant l'horloge par 1024.

	for(;;) {}
	return 0;
}
