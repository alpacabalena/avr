/************************************************************************************************************
 * Le programme est un thermostat. Il permet de mesure la valeur d'un voltage qui varie en fonction de la
 * température d'un thermistor. La valeur cible est contrôlée par un potentiomètre. L'affichage des valeurs
 * numériques se fait sur 10 DELs rouges. Lorsqu'il fait trop chaud, un relais sur PB0 ou PB1 s'active pour
 * allumer une lumière incandescente installée proche d'un climatiseur conventionnel. Le programme est conçu
 * pour un ATmega16. Il y a une image dans le même dossier que ce code qui montre le circuit.
 * 
 * Pour comprendre la variable CORRECTION_POTENTIOMETRE_1, il faut comprendre le circuit physique.
 *
 * J'utilise au maximum les interruptions dans ce programme pour donner une idée de ce qu'il est posible
 * d'accomplir avec elles et démontrer comment les configurer.
 *
 * Pourquoi ne pas utiliser des valeurs uint16_t pour le voltage du potentiomètre et du thermistor? J'ai
 * essayé et les valeurs affichées sur les DELs rouges ne changeaient plus. On pourrait déboguer plus pour
 * comprendre. Mon hypothèse principale c'est qu'avec toutes les interruptions, il doit se passer quelque
 * chose qui perturbe l'arithmétique 16 bits. L'ATmega16 est un microcontrôleur 8 bits à la base.
 *
 * Il y a d'autres informations techniques à la fin du programme après la fonction main.
 *
 * Copyright (C) 2022 Alpaca Balena.
 *
 * Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
 * Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
 * licence, soit (à vous de voir...) toute autre version ultérieure.
 *
 * Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
 * implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
 * connaissance de la Licence Publique Générale GNU pour plus de détails.
 *
 * Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
 * cas, voir <https://www.gnu.org/licenses/>.
************************************************************************************************************/
#include <avr/io.h> 
#include <avr/interrupt.h>

/* Constantes. J'aurais pu utiliser des DEFINEs, mais j'ai préféré utiliser des variables en MAJUSCULES. */
// Correction de la valeur du voltage pour l'amener plus haut pour permettre des comparaisons qui produiront
// un allumage de l'ampoule à des températures plus basses.
uint8_t CORRECTION_POTENTIOMETRE_1 = 143;  // Bits de poids faible.
uint8_t CORRECTION_POTENTIOMETRE_2 = 1;  // Bits de poids fort
uint8_t DEFAUT_COMPTEUR_ACTIF = 25; // n fois la minuterie 2 de 20 ms.
uint8_t DEFAUT_COMPTEUR_ROUTINE = 5; // n fois la minuterie 0 de 20 ms.
uint8_t DEFAUT_COMPTEUR_SIGNAL = 5; // n fois la minuterie 1 de 1 seconde.

/* Variables globales. Certaines sont des mesures des ADCs et d'autres sont des états de machines à états 
 *   Les machines à état sont décrites dans des diagrammes dans le même dossier que ce code. */
uint8_t attente_routine_test = DEFAUT_COMPTEUR_ROUTINE;
uint16_t test_del = 1;  // Tester les DELs.
uint8_t choix_lecture = 0;  // 0 ADC0, 1 ADC1, en alternance.
uint8_t compteur_actif = DEFAUT_COMPTEUR_ACTIF;
uint8_t compteur_signal = DEFAUT_COMPTEUR_SIGNAL;
uint8_t mode = 0;  // Mode 0 temperature.
uint8_t voltage_potentiometre_1 = 0;
uint8_t voltage_potentiometre_2 = 0;
uint8_t voltage_thermistor_1 = 0;  // Initialisons notre lecture à 0.
uint8_t voltage_thermistor_2 = 0;  // Initialisons notre lecture à 0.

/* Afficher la valeur sur toutes les dels, même la verte. 
 * Du bit de poids fort au bit de poids faible, on a: PA6 PA7 PC7 PC6 PC5 PC4 PC3 PC2 PC1 PC0 PD7.
 * À n'utiliser que pour la routine de départ!! Après la routine, il y a des problèmes avec l'arithmétique
 * 16 bits se. */
void afficher_dels(uint16_t valeur)
{
    // Remettre les DELs à 0.
    PORTD &= ~(1 << PD7);
    PORTA &= (~(1 << PA6)) & (~(1 << PA7));
    // Afficher la valeur.
    PORTD |= (valeur & 1) << PD7;
    PORTC = valeur >> 1;
    // PA6.
    PORTA |= ((valeur >> 10) & 1) << PA6;
    // PA7.
    PORTA |= ((valeur >> 9) & 1) << PA7;
}

/* Routine lors de l'interruption INT0. On active le mode thermistor. On désactive l'interruption INT0.
 * On désactive l'interruption sur INT0 pour la réactiver plus tard dans l'interruption de la minuterie 1. 
 * Ça évite de bloquer le reste du programme par des interruptions si on laisse le commutateur connecter la
 * terre à la patte INT0 du microcontrôleur. */
ISR(INT0_vect)
{
    mode = 0;
    GICR &= ~(1 << INT0);
}

/* Routine lors de l'interruptoin INT1. On active le mode potentiomètre. On désactive l'interruption INT1.
 * Même raisons que pour INT0. */
ISR(INT1_vect)
{
    mode = 1;
    GICR &= ~(1 << INT1);
}

/* Routine lors de l'interruption d'une valeur analogique convertie prête à être lue.
 * Utilisé pour mesurer la valeur de voltage au potentiomètre et au thermistor en alternance. Affiche aussi
 * l'un ou l'autre des voltage sur 10 DELs rouges en fonction du mode sélectionné par un commutateur. */
ISR(ADC_vect)
{
    // Mesure des voltages en alternance.
    if (choix_lecture == 1)
    {   // On regarde ADC1 pour lire le voltage au potentiomètre.
        voltage_potentiometre_1 = ADCL;
        voltage_potentiometre_2 = ADCH;
        // Ajout de 10 à la valeur de voltage lue pour corriger un problème
        // électrique.
        if (0xff - voltage_potentiometre_1 < CORRECTION_POTENTIOMETRE_1){
            voltage_potentiometre_2 += 1;
        }
        voltage_potentiometre_1 += CORRECTION_POTENTIOMETRE_1;
        voltage_potentiometre_2 += CORRECTION_POTENTIOMETRE_2;
        ADMUX = 0x40;
        choix_lecture = 0;
    }
    else
    {   // On regarde ADC0 pour lire le voltage au thermistor.
        voltage_thermistor_1 = ADCL;
        voltage_thermistor_2 = ADCH;
        ADMUX = 0x41;
        choix_lecture = 1;
    }

    // Affichage du voltage mesuré sur les DELs rouges selon le mode.
    // Il y a du code dupliqué ici qu'on pourrait simplifier.
    uint8_t masque = (1 << PA6) | (1 << PA7);
    if (mode == 1)
    {
        uint8_t voltage_2 = voltage_potentiometre_2 & 1;
        voltage_2 = voltage_2 << 1;
        voltage_2 |= (voltage_potentiometre_2 & 2) >> 1;
        PORTC = voltage_potentiometre_1;
        PORTA = (PORTA & ~masque) | ((voltage_2 << PA6) & masque);
    }
    else
    {
        uint8_t voltage_2 = voltage_thermistor_2 & 1;
        voltage_2 = voltage_2 << 1;
        voltage_2 |= (voltage_thermistor_2 & 2) >> 1;
        PORTC = voltage_thermistor_1;
        PORTA = (PORTA & ~masque) | ((voltage_2 << PA6) & masque);
    }
    ADCSRA = 0xcf;  // Activation du comparateur et de son interruption.
}


/* Routine lorsque la minuterie 1 atteint sa valeur de comparaison A.
 * Cette comparaison devrait être configurée pour se déclencher à chaque seconde. */
ISR(TIMER1_COMPA_vect)
{
    GICR = (1 << INT0) | (1 << INT1);
    // Attendons le nombre de fois du compteur_signal avant d'évaluer la condition
    if (compteur_signal > 0)
    {
        compteur_signal--;
        return;
    }
    else
    {
        compteur_signal = DEFAUT_COMPTEUR_SIGNAL;
    }
    // Calculer si on doit allumer ou éteindre le climatiseur.
    // On utilise un thermistor NTC de 10 000 ohms. Quand il se réchauffe,
    // la valeur lue diminue. Quand il se refroidit, la valeur lue augmente.
    uint8_t allumer = 0x00;
    uint8_t signal_et_voyant = (1 << PB1) | (1 << PB2);
    if (voltage_thermistor_2 < voltage_potentiometre_2)
    {
        allumer = signal_et_voyant;
    }
    else if (voltage_thermistor_2 == voltage_potentiometre_2)
    {
        if (voltage_thermistor_1 < voltage_potentiometre_1)
        {
            allumer = signal_et_voyant;
        }
    }

    // Allumer ou éteindre l'ampoule qui contrôle le climatiseur en utilisant 
    // PB0 et PB1.
    PORTB = (PORTB & ~signal_et_voyant) | allumer;
}

/* Routine d'interruption qui fait clignoer la DEL verte.
 * La minuterie 2 est de 8 bits. On a besoin d'un compteur pour suivre le nombre de fois où elle se déclenche
 * et inverser la DEL verte à la période de notre choix. */
ISR(TIMER2_COMP_vect)
{
    compteur_actif--;
    if (compteur_actif == 0)
    {
        compteur_actif = DEFAUT_COMPTEUR_ACTIF;
        // Inverser seulement PD7 pour signaler que le système est en vie.
        PORTD = (PORTD & ~(1 << PD7)) | ((~PORTD) & (1 << PD7));
    }
}

/* Routine de test avec la minuerie 0. 
 * On décalle un uint16_t qu'on affiche sur les 10 DELs rouges et la DEL verte. À la fin du test, on
 * configure les minuteries et le convertisseur analogique numérique pour entrer dans la machine à état
 * principale du programme qui mesure et compare les valeurs numériques pour déclencher un relais. */
ISR(TIMER0_COMP_vect)
{
    if (attente_routine_test > 0)
    {
        attente_routine_test--;
        return;
    }
    // Verifier si on affecte les ports.
    if (test_del < 0x0800)
    {
        afficher_dels(test_del);
        // On prépare la valeur pour le prochain cycle.
        test_del = test_del << 1;
        attente_routine_test = DEFAUT_COMPTEUR_ROUTINE;
    }
    else
    {
        TCCR0 = 0;  // Désactivation de la minuterie 0.
    /* Configuration du compteur Timer1. */
        // Activation de l'interruption du Timer1 sur comparaison. Lorsque la valeur
        // du registre de comparaison A est atteinte, l'interruption est lancée.
        TIMSK = (1 << OCIE1A); // désactive à la fois la minuterie 0.

        // 12MHz/1024 = 11 719 impulsions par 1 seconde.
        // On configure la valeur dans le registre de comparaison A.
        // Pourquoi diviser par 1024? Pour avoir des valeurs plus petites dans le
        // registre de comparaison A. La division par 1024 est faite quand on
        // configure TCCR1B plus bas.
        OCR1A = 11719;

        TCCR1A = (1 << COM1A0);  // Activation de la comparaison.
        // Démarrer le Timer1 et diviser l'horloge par 1024.
        // À chaque coup d'horloge divisée, le compteur augmentera de 1.
        TCCR1B = (1 << WGM12) | (1 << CS12) | (1 << CS10);
    /* Fin de la configuration du compteur Timer1. */

    /* Configuration du timer 2 pour signaler la vie du micro-contrôleur. */
        // Activation de l'interruption pour la minuterie 2.
        TIMSK |= TIMSK | (1 << OCIE2);

        // WGM21 = 1 et WGM20 = 0 automatiquement remettre le compteur à 0 (CTC).
        // COM21:20 à 00 pour garder PD7 en mode normal.
        // CS22:20 à 111 divise l'horloge de 12 MHz par 1024.
        TCCR2 = (1 << WGM21) | (1 << CS22) | (1 << CS21) | (1 << CS20);
        // 12 MHz / 1024 = 11 719 impulsions par 1 seconde.
        OCR2 = 234;  // Environ 0,02 seconde, soit 20 ms.
    /* Fin de la configuration de la minuterie 0. */

    /* Configuration du comparateur analogique. */
        ADMUX = 0x40;  // Sélection du canal de comparaison.
        ADCSRA = 0xcf;  // Activation du comparateur et de son interruption.
    /* Fin de la configuration du comparateur analogique. */
    /* Configuration de INT0 et INT1. */
        GIFR = 0x00;
        GICR = (1 << INT0) | (1 << INT1);
    /* Fin de la configuration INT0 et INT1. */
    }
}

int main()
{
/* Desactivation du JTAG */
    MCUCSR |= (1 << JTD);
    MCUCSR |= (1 << JTD);
    MCUCSR |= (1 << JTD);

/* Configuration des pattes en sorties et entrées. */
    DDRA = (1 << PA6) | (1 << PA7);  // PA6 et PA7 en sortie, le reste en entrée.
    DDRB = (1 << PB0) | (1 << PB1);  // PB1 et 2 en sortie, le reste en entrée.
    DDRC = 0xff;  // Toutes les pattes de PC0 à PC7 en sortie.
    DDRD = (1 << PD7);  // PD7 en sortie, les autres en entrée.

/* Routine de test */
/* Configuration du timer 0 pour la routine de test. */
    // Activation de l'interruption pour la minuterie 0.
    TIMSK |= (1 << OCIE0);

    // WGM01 = 1 et WGM00 = 0 pour automatiquement remettre le compteur à 0.
    // COM01:00 à 00 pour garder PB3 en mode normal.
    // CS02:00 à 101 divise l'horloge de 12 MHz par 1024.
    TCCR0 = (1 << WGM01) | (1 << CS02) | (1 << CS00);
    // 12 MHz / 1024 = 11 719 impulsions par 1 seconde.
    OCR0 = 234;  // Environ 0,02 seconde, soit 20 ms.
/* Fin de la configuration de la minuterie 0. */
    sei();  // Activation des interruptions.
/* Fin routine de test */

    for(;;) { /* Ne faisons rien. Les interruptions vont tout faire. */ }

    return 0;
}
/* ISR déclare une section de code à exécuter lorsqu'une interruption est lancée. */
/*
ADCSRA
|   7   |   6   |   5   |   4   |   3   |   2   |   1   |   0   |
| ADEN  | ADSC  | ADATE | ADIF  | ADIE  | ADPS2 | ADPS1 | ADPS0 |
ADEN: Bit d'activation du convertisseur. Mettre à 1 pour activer.
ADSC: Bit de déclenchement de conversion. Mettre à 1 pour déclencher.
ADATE: Bit de déclenchement automatique. Mettre à 1 pour l'utiliser.
ADIF: Bit lié à l'interruption qui indique qu'une valeur est prête à être lue.
ADIE: Bit d'activation de l'interruption qui indique qu'une valeur est prête.
ADPS2 à 0: Division de la fréquence d'horloge. On doit diviser la fréquence pour
      donner au convertisseur une fréquence de moins de 200 kHz.

Si on veut fonctionnner sans les interruptions:
0x87 == 0x1000 0111
0x87 == (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0)

Si on veut les interruptions:
0x8f == (1 << ADEN) | (1 << ADIE) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0)

Si on veut démarrer une conversion avec les interruptions:
0xcf = (1 << ADEN) | (1 << ADSC) | (1 << ADIE) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0)

Valeurs de ADPS2 à ADPS0:
| 2:0 | division |
| 000 |    2     |
| 001 |    2     |
| 010 |    4     |
| 011 |    8     |
| 100 |    16    |
| 101 |    32    |
| 110 |    64    |
| 111 |    128   |
*/

/*
ADMUX
|   7   |   6   |   5   |   4   |   3   |   2   |   1   |   0   |
| REFS1 | REFS0 | ADLAR | MUX4  | MUX3  | MUX2  | MUX1  | MUX0  |

REFS1:0
|1:0 | résultat                                                   |
| 00 | La patte AREF donne le voltage de référence.               |
| 01 | La patte AVCC donne le voltage de référence, donc 5 volts. |
| 10 | Réservé. Ne pas utiliser.                                  |
| 11 | Valeur interne 2. (Pas certain de ce que ça fait...)       |
ADLAR
1: Pour ajuster les bits à gauche sur ADCH et ADCL.
0: Pour ajuster les bits à droite sur ADCH et ADCL.
MUX4:0
Simplement entrer la valeur du canal à lire. Si ADC0, MUX4:0 == 0000.

Pour utiliser ADC0 avec AVCC comme voltage de référence et ajuster les bits à
droite:
0x40 == (1 << REFS0)

Pour utiliser ADC1 avec AVCC comme voltage de référence et ajuster les bits à drotie:
0x41 == (1 << REFS0) | (1 << MUX0)
*/
